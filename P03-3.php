<?php
$juego = true;
$randint = rand(0, 10);
$intentos = 5;
$entrada = readline('Introdueix un número del 0 al 10 des de l\'entrada standard: ');
while ($juego != false) {
    if (is_numeric($entrada)) {
        if ($intentos == 0) {
            $intentos = 0;
            echo 'Te has quedado sin intentos' . "\n";
            $juego = false;
        } elseif ($entrada == $randint) {
            $intentos = 0;
            echo 'Has ganado' . "\n";
            $juego = false;
        } elseif ($entrada > $randint) {
            echo 'Te has pasado' . "\n";
            $intentos = $intentos - 1;
            echo 'Te quedan ' . $intentos . ' Tiradas' . "\n";
            $entrada = readline('Introdueix un número del 0 al 10 des de l\'entrada standard: ');
        } elseif ($entrada < $randint) {
            echo 'Te has quedado corto' . "\n";
            $entrada = readline('Introdueix un número del 0 al 10 des de l\'entrada standard: ');
            $intentos = $intentos - 1;
            echo 'Te quedan ' . $intentos . ' Tiradas' . "\n";
        } else {
            echo 'No es un numero' . "\n";
            $intentos = $intentos - 1;
            echo 'Te quedan ' . $intentos . ' Tiradas' . "\n";
            $entrada = readline('Introdueix un número del 0 al 10 des de l\'entrada standard: ');
        }
    }
}
?>
            