<?php
        $ancho_maximo = 10;
        $fila = 1;
        $asteriscos = 1;

        while ($fila <= $ancho_maximo) {
            echo str_repeat('&nbsp;', ($ancho_maximo - $asteriscos));
            echo str_repeat('*', $asteriscos);
            echo '<br>';
            $asteriscos += 1;
            $fila++;
        }
    ?>